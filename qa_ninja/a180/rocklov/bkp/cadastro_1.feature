#language: pt
Funcionalidade: Cadastro

    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

@cadastro
Cenario: Fazer cadastro

    Dado que acesso a página de cadastro
    #Quando submeto o meu cadastro completo
    Quando submeto o seguinte formulário de cadastro:
    |nome|email|senha|
    |Cristiano Lobo| cristiano.coll@gmail.com|88973526|
    Então sou redirecionado para o Dashboard

@tentativa_cadastro_1
Cenario: Submeter cadastro sem o nome

    Dado que acesso a página de cadastro
    #Quando submeto o meu cadastro sem o nome
    Quando submeto o seguinte formulário de cadastro:
    |nome|email|senha|
    || cristiano.coll@gmail.com|88973526|
    Então vejo a mensagem de alerta: "Oops. Informe seu nome completo!"

@tentativa_cadastro
Cenario: Submeter cadastro sem o email

    Dado que acesso a página de cadastro
    #Quando submeto o meu cadastro sem o email
    Quando submeto o seguinte formulário de cadastro:
    |nome|email|senha|
    |Cristiano Lobo||88973526|
    Então vejo a mensagem de alerta: "Oops. Informe um email válido!"

@tentativa_cadastro
Cenario: Submeter cadastro com email incorreto

    Dado que acesso a página de cadastro
    #Quando submeto o meu cadastro com incorreto
    Quando submeto o seguinte formulário de cadastro:
    |nome|email|senha|
    |Cristiano Lobo| cristiano.coll*gmail.com|88973526|
    Então vejo a mensagem de alerta: "Oops. Informe um email válido!"

@tentativa_cadastro
Cenario: Submeter cadastro sem a senha

    Dado que acesso a página de cadastro
    #Quando submeto o meu cadastro sem a senha
    Quando submeto o seguinte formulário de cadastro:
    |nome|email|senha|
    |Cristiano Lobo| cristiano.coll@gmail.com||
    Então vejo a mensagem de alerta: "Oops. Informe sua senha secreta!"
