#language: pt
 
Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema Rocklov
    Para que eu possa anunciar meus equipamentos musicasis

  @login
    Cenário: Login do  usuário

        Dado que acesso a página principal
        Quando submeto minhas credencias com "cristiano@gmail.com" e "123456"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credencias com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input              | senha_input | mensagem_output                  |
            | cristiano.coll@gmail.com | 111         | Usuário e/ou senha inválidos.    |
            | cristiano.coll@404.com   | 88973526    | Usuário e/ou senha inválidos.    |
            | cristiano.coll*gmail.com | 111         | Oops. Informe um email válido!   |
            | cristiano.coll@gmail.com | 111         | Usuário e/ou senha inválidos.    |
            |                          | 88973526    | Oops. Informe um email válido!  |
            | cristiano.coll@gmail.com |             | Oops. Informe sua senha secreta! |

