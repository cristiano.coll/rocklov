require 'capybara'
require 'capybara/cucumber'
require 'webdrivers'
require 'selenium-webdriver'
require 'faker'
require 'mongo'

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :chrome_headless do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--window-size=1920,1080')
  options.add_argument('--headless')
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')

  Capybara::Selenium::Driver.new(app,
                                 browser: :chrome,
                                 options: options)
end

Capybara.configure do |config|
  # config.default_driver = :selenium_chrome
  Capybara.default_driver = :chrome_headless
  config.app_host = 'http://rocklov-web:3000'
  config.default_max_wait_time = 15
end
