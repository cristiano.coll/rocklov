class EquiposPage 
    include Capybara::DSL
    def create(equipo)
        #checkpoint com timeout explicit
        page.has_css?("#equipoForm")

        thumb = Dir.pwd + '/features/support/fixtures/images/' + equipo[:thumb]
        find("#thumbnail input[type='file']", visible: false).set thumb
        find('input[placeholder$=equipamento]').set equipo[:nome]
        select_cat(equipo[:categoria]) if equipo[:categoria].length > 0 
        find("input[placeholder^='Valor']").set equipo[:preco]
        click_button 'Cadastrar'
    end

    def select_cat(cat)
        find("select[id='category']").find('option', text: cat).select_option
    end

    def upload(file_name)
        thumb = Dir.pwd + "/features/support/fixtures/imagens/" + file_name
        find("#thumbnail input[type=file]", visible: false).set thumb
    end    
end

