class LoginPage
    include Capybara::DSL # com essa linha a classe loginpage passa a reconhecer todos os recursos do capybara

    def open
        visit "/"
    end

    def with(email, password)
        find("input[placeholder='Seu email']").set email
        find("input[type=password]").set password
        click_button "Entrar"
    end

    def alert_dark
        return find(".alert-dark").text #devolve o elemento do tipo texto
    end
end
