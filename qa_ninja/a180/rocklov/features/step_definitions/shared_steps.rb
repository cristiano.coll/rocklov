Então('sou redirecionado para o Dashboard') do
    #expect(page).to have_css ".dashboard"
    expect(@dash_page.on_dash?).to be true
end

Então('vejo a mensagem de alerta: {string}') do |expect_alert|

    #Antes da refatoração usando PO
    #alert = find(".alert-dark")
    #expect(alert.text).to eql expect_alert
    #expect(@alert.dark).to eql expect_alert

    #Apos a refatoração
    #expect(@login_page.alert_dark.text).to eql expect_alert
    #expect(@login_page.alert_dark).to eql expect_alert
    expect(@alert.dark).to eql expect_alert #usando componente @alert
end
