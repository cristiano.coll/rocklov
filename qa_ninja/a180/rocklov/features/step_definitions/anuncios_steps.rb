Dado('Login com {string} e {string}') do |email, password|
  @email = email
  @login_page.open  
  @login_page.with(email, password)
end

#Dado('que estou logado como {string} e {string}') do |email, password|
  #visit "/"
  #find("input[placeholder='Seu email']").set email
  #find("input[type=password").set password
  #click_button "Entrar"
  #login_page.open
  #login_page.with(email, password)
  #sleep(2)
#end

Dado('que acesso o formulario de cadastro de anúncios') do
  @dash_page.goto_equipo_form
  # isso é um checkpoint para garantir que eu estou no lugar correto
  #expect(page).to have_css '#equipoForm'
end

Dado('que eu tenho o seguinte equipamento:') do |table|
  # variavel de instancia
  @anuncio = table.rows_hash
  MongoDB.new.remove_equipo(@anuncio[:nome], @email)
  log @anuncio
end

Quando('submeto o cadastro desse item') do
  @equipos_page.create(@anuncio)
end

Então('devo ver esse item no meu Dashboard') do
  #anuncios = find(".equipo-list")
  # expect(anuncios).to have_content @anuncio[:nome]
  # expect(anuncios).to have_content "R$#{@anuncio[:preco]}/dia"
  expect(@dash_page.equipo_list).to have_content @anuncio[:nome]
  expect(@dash_page.equipo_list).to have_content "R$#{@anuncio[:preco]}/dia"
end

Então('deve conter a mensagem de alerta: {string}') do |expect_alert|
  expect(@alert.dark).to have_text expect_alert
end


