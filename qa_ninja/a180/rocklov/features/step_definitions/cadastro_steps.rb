#require 'mongo'

Dado('que acesso a página de cadastro') do
  #visit "/signup"
  @signup_page.open
end

#antigo antes de implementar o uso da table
#Quando('submeto o meu cadastro completo') do
#   MongoDB.new.remove_user("cristiano.coll@gmail.com")
#   find("#fullName").set "Cristiano Lobo"
#   find("#email").set "cristiano.coll@gmail.com"
#   #find("#email").set Faker::Internet.free_email
#   find("#password").set "88973526"
#   click_button "Cadastrar"
# end

Quando('submeto o seguinte formulário de cadastro:') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  user = table .hashes.first
  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)

  #Antes da refatoração 
  #find("#fullName").set user[:nome]
   #find("#email").set user[:email]
  # #find("#email").set Faker::Internet.free_email
   #find("#password").set user[:senha]
   #click_button "Cadastrar"
end



# Quando('submeto o meu cadastro sem o nome') do
#   find("#email").set Faker::Internet.free_email
#   find("#password").set "88973526"
#   click_button "Cadastrar"
# end

# Quando('submeto o meu cadastro sem o email') do
#   find("#fullName").set "Cristiano Lobo"
#   find("#password").set "88973526"
#   click_button "Cadastrar"

# end

# Quando('submeto o meu cadastro com incorreto') do
#   find("#fullName").set "Cristiano Lobo"
#   find("#email").set"cristiano*gmail.com"
#   find("#password").set "88973526"
#   click_button "Cadastrar"
# end

# Quando('submeto o meu cadastro sem a senha') do
#   find("#fullName").set "Cristiano Lobo"
#   find("#email").set"cristiano.coll@gmail.com"
#   find("#password").set ""
#   click_button "Cadastrar"
# end

# Então('vejo a mensagem de alerta: {string}') do |expect_alert|
#   alert = find(".alert-dark")
#   expect(alert.text).to eql expect_alert
# end

# Então('sou redirecionado para o Dashboard') do
#   expect(page).to have_css ".dashboard"
# end





