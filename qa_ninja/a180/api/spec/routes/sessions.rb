#require "httparty" #importando o httparty
require_relative "base_api"

class Sessions < BaseApi #está herdando as caracteristicas do include HTTParty e a base_uri
#include HTTParty #implementando o httparty
#base_uri "http://rocklov-api:3333"

    def login(payload)
            return self.class.post(   #self.class é pra poder ter acesso aos objetos da própria classe
                "/sessions",
                 body: payload.to_json,
                 headers: {
                     "Content-Type": "application/json",
                     
                 },
                 )
    end

end
