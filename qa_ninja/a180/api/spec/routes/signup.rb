#require "httparty" #importando o httparty
require_relative "base_api"

class Signup < BaseApi
#include HTTParty #implementando o httparty
#base_uri "http://rocklov-api:3333"

    def create(payload)
            return self.class.post(   #self.class é pra poder ter acesso aos objetos da própria classe
                "/signup",
                 body: payload.to_json,
                 headers: {
                     "Content-Type": "application/json",
                     
                 },
                 )
    end

end
